#Text to Form

Text to Form is a simple way to convert an easily understandable text file into a HTML formatted form.

Live example: https://www.alsmind.co.uk/text-to-form/

A string could look something like:

```txt
/* form params */
form->action:
form->method:post

/* standard input */
input->legend:Contact Details
input->type:text
input->name:first-name
input->label:First Name
input->autocomplete:given-name
input->data-required:
input->hint:Please enter your first name

/* standard input */
input->type:text
input->name:last-name
input->label:Last Name
input->autocomplete:family-name
input->data-required:
input->hint:Please enter your last name

/* email input */
input->type:email
input->name:email
input->label:Email Address
input->autocomplete:email
input->hint:Please enter a valid email address for example: john.doe@some-domain.com


/* selector */
select->legend:Your Nationality
select->name:country
select->label:Select Country
select->option:UK
select->option:DE
select->option:FR
select->option:US


/* buttons */
button->legend:Submission options
button->value:Ajax
button->type:button
button->id:ajax-request
button->data-action:submit_form

button->type:submit
button->value:Submit

button->type:reset
button->value:Reset
```

##Example

There is an hello world example included in the source. If you are famaliar with PHP this exmaple will give you a good overview of how to use the class.


##More to come

Full documentation for this class to follow.