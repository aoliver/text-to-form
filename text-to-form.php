<?php
/*
    text to Form
    @license: MIT - http://opensource.org/licenses/MIT
    @version: 1.0.0
    @environment: Development
    @author: Alex Oliver
    @Repo: 
*/
    class text_to_form{

        var $working_array = [],
        $defaults;

        function __construct($input_string = '', $options = []){
            
            //set defaults
            $defaults = [
                'show_validation_errors' => true,
                'input_container_tag' => 'p'
            ];

            //merge and overwrite defaults
            $this->defaults = array_merge($defaults, $options);

            if(strlen($input_string) > 0){
                $this->process($input_string);
            }

            return $this;
        }

        public function set_options($options = []){
            //merge and overwrite defaults
            $this->defaults = array_merge($this->defaults, $options);
        }

        public function process($input_string = ''){

            $this->working_array = [
                'form' => [],
                'fieldsets' => []
            ];
            
            foreach($this->filter_params_array($input_string) as $fs_key => $fieldsets){
                
                $this->working_array['fieldsets'][$fs_key] = [];

                foreach($fieldsets as $this_row){

                    $this_row['form'] = (isset($this_row['form']) ? $this_row['form'] : false);

                    if($this_row['form']){
                        $this->working_array['form'] = $this_row['form'];
                    } else {
                        $type_index = array_keys($this_row);
                        $this_type = $type_index[0];

                        $label = (isset($this_row[$this_type]['label']) ? $this_row[$this_type]['label'] : false);
                        
                        if($label){
                            unset($this_row[$this_type]['label']);
                            $this_row[$this_type]['id'] = (!isset($this_row[$this_type]['id']) ? (isset($this_row[$this_type]['name']) ? $this_row[$this_type]['name'] : false) : $this_row[$this_type]['id']);
                        }

                        $this->working_array['fieldsets'][$fs_key][] = [
                            $this_type => [
                                'label' => [
                                    'label' => $label,
                                    'for' => (isset($this_row[$this_type]['id']) ? $this_row[$this_type]['id'] : false)
                                ],
                                'params' => $this_row[$this_type]
                            ]
                        ];
                    }
                }

            }

            return $this;
        }

        private function filter_params_array($input_string = ''){
            //remove comments
            $input_string = preg_replace('/\/\*(.*?)*\//s', '', $input_string);

            //fieldsets
            $fieldsets = explode("\n\n\n\n", $input_string);

            //fieldset array
            $fieldset_array = [];

            foreach($fieldsets as $key => $this_fieldset){

                //break into elements
                $working_array = explode("\n\n", $this_fieldset);

                //generate paramaters
                $working_array = array_map(function($e){
                    
                    $params = [];

                    foreach(explode("\n", $e) as $this_param){
                        if(preg_match_all('/(.*?):(.*)/', $this_param, $matches, PREG_SET_ORDER)){
                            
                            //params
                            $this_params = (isset($matches[0]) ? $matches[0] : $matches);

                            //input type
                            $param_split = explode('->', $this_params[1]);
                            $type = strtolower(trim($param_split[0]));

                            if($this->valid_fields($type)){

                                //param
                                $param = str_replace(' ', '->', strtolower(trim($param_split[1])));

                                //value
                                $value = trim($this_params[2]);

                                //type exists in array
                                $params[$type] = (isset($params[$type]) ? $params[$type] : []);

                                //has options
                                if($param === 'option'){
                                    $params[$type]['options'] = (isset($params[$type]['options']) ? $params[$type]['options'] : []);
                                    $params[$type]['options'][] = $value;
                                } else {
                                    //add param and value
                                    $params[$type][$param] = $value;
                                }
                                
                            }
                        }
                    }

                    return ($params ? $params : []);

                }, $working_array);

                $fieldset_array[] = $working_array;
            }

            return $fieldset_array;
        }

        private function valid_fields($field_name = ""){
            return in_array($field_name, ['form','input','button','textarea','select','meter','progress','datalist']);
        }

        public function render(){
            echo $this->inputs_to_html();
        }

        private function inputs_to_html(){
            
            $export_string = '';

            $c_tag_close = (isset($this->defaults['input_container_tag']) ? '</'.$this->defaults['input_container_tag'].'>' : '');

            //each fieldset
            foreach($this->working_array['fieldsets'] as $fs_key => $fieldset){

                $export_string .= '<fieldset data-key="'.($fs_key + 1).'">'."\n";

                //each input
                foreach($fieldset as $params){
                    $html_params = '';
                    $options = '';
                    $type_index = array_keys($params);
                    $type = $type_index[0];

                    $c_tag_open = (isset($this->defaults['input_container_tag']) ? '<'.$this->defaults['input_container_tag'].' data-input-type="'.(isset($params[$type]['params']['type']) ? $params[$type]['params']['type'] : $type).'">' : '');


                    //parameters
                    if(isset($params[$type]['params'])){

                        $hint = '';
                        $legend = '';
                        $select_tabbed = false;
                        $select_options = [];
                        $button_type = 'button';

                        foreach($params[$type]['params'] as $param_key => $param_value){
                            
                            switch($param_key){
                            case 'options':
                                $checkbox_name = (isset($params[$type]['params']['name']) ? $params[$type]['params']['name'].'_' : 'checkbox_');
                                $checkbox_count = 0;
                                $new_checkbox_name = '';
                                
                                foreach($param_value as $option_value){

                                    $params[$type]['params']['type'] = (isset($params[$type]['params']['type']) ? $params[$type]['params']['type'] : '');

                                    switch($params[$type]['params']['type']){
                                    case 'checkbox':
                                    case 'radio':

                                        //ensure checkbox names are not the same
                                        if($params[$type]['params']['type'] === 'checkbox'){
                                            $checkbox_count += 1;
                                            $new_checkbox_name = ' name="'.$checkbox_name.$checkbox_count.'"';
                                        }

                                        $options .= '<label>'.$option_value.' <input type="'.$params[$type]['params']['type'].'" {the_params}value="'.$option_value.'"'.$new_checkbox_name.' /></label>';
                                        
                                        break;
                                    default:
                                        $options .= '<option value="'.$option_value.'">'.$option_value.'</option>';
                                        $select_options[] = '<button type="button" data-select-tab="true" value="'.$option_value.'">'.$option_value.'</button>';
                                    }
                                }
                                break;
                            case 'name':
                                $params[$type]['params']['type'] = (isset($params[$type]['params']['type']) ? $params[$type]['params']['type'] : '');

                                if($params[$type]['params']['type'] !== 'checkbox'){
                                    $html_params .= $param_key.'='.'"'.$param_value.'" ';
                                }
                                break;
                            case 'required':
                                $html_params .= $param_key.' ';
                                break;
                            case 'hint':
                                $hint = '<span class="hint">'.$param_value.'</span>';
                                break;
                            case 'legend':
                                $legend = '<legend>'.$param_value.'</legend>';
                                break;
                            case 'display':
                                $select_tabbed = (strtolower(trim($param_value)) === 'tabbed' ? true : false);
                                break;
                            case 'type':
                                if($type === 'button'){
                                    $button_type = $param_value;
                                } else {
                                    $html_params .= $param_key.'='.'"'.$param_value.'" ';
                                }
                                break;
                            default:
                                $html_params .= $param_key.'='.'"'.$param_value.'" ';
                            }
                        }

                        $the_label = ($params[$type]['label']['label'] ? '<label for="'.$params[$type]['label']['for'].'">'.$params[$type]['label']['label'].'</label>' : '');

                        //input type
                        switch($type){
                        case 'input':
                            switch($params[$type]['params']['type']){
                            case 'checkbox':
                            case 'radio':
                                $export_string .= $legend.$c_tag_open.$the_label.str_replace('{the_params}', $html_params, $options).$hint.$c_tag_close."\n";
                                break;
                            case 'hidden':
                                $export_string .= '<input '.$html_params.'/>'."\n";
                                break;
                            default:
                                $export_string .= $legend.$c_tag_open.$the_label.'<input '.$html_params.'/>'.$hint.$c_tag_close."\n";
                            }
                            
                            break;
                        case 'textarea':
                            $export_string .= $legend.$c_tag_open.$the_label.'<textarea '.$html_params.'></textarea>'.$hint.$c_tag_close."\n";
                            break;
                        case 'button':
                            $c_tag_open = (isset($this->defaults['input_container_tag']) ? '<'.$this->defaults['input_container_tag'].' data-input-type="button">' : '');
                            $export_string .= $c_tag_open.$the_label.'<button '.$html_params.' type="'.$button_type.'">'.(isset($params[$type]['params']['value']) ? $params[$type]['params']['value'] : '').'</button>'.$c_tag_close."\n";
                            break;
                        case 'select':
                            if($select_tabbed && count($select_options) > 0){
                                $export_string .= $legend.$c_tag_open.$the_label.'<span class="select-tabs">'.implode('', $select_options).'<select '.$html_params.' multiple>'.$options.'</select></span>'.$hint.$c_tag_close."\n";
                            } else {
                                $export_string .= $legend.$c_tag_open.$the_label.'<select '.$html_params.'>'.$options.'</select>'.$hint.$c_tag_close."\n";
                            }

                            break;
                        }
                    }
                }

                $export_string .= '</fieldset>'."\n";
            }

            //form wrap
            $form_params = '';
            foreach($this->working_array['form'] as $form_param_key => $form_param_value){
                $form_params .= $form_param_key.'='.'"'.$form_param_value.'" ';
            }

            //show form validation errors
            $validation_errors = ($this->defaults['show_validation_errors'] ? '<ul class="form-error-list"></ul>' : '');

            $export_string = '<form '.trim($form_params).'>'."\n".$export_string.$validation_errors.'</form>';

            //return 
            return $export_string;
        }
    }
?>