<?php
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
?>
<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=10, user-scalable=yes" />
	<link rel="manifest" href="/example/manifest.json">
	<meta name="description" content="Hello World text to form example"/>

	<title>Form Test</title>

	<!-- simple form styling -->
	<link rel="stylesheet" type="text/css" href="../tools/form.css?v=<?php echo filectime('../tools/form.css'); ?>">

</head>

<body role="main">

	<h1 style="text-align:center;">Hello World</h1>

<?php
	//include class
	include '../text-to-form.php';

	//create new instance
	$this_form = new text_to_form;
	
	//set form options
	$this_form->set_options([
		'show_validation_errors' => true, //true by default
		'input_container_tag' => 'p' //input container. P tag is default
	]);

	//render form
	echo $this_form->process(file_get_contents('example.txt'))->render();
?>

	<!-- form interaction and validation script -->
	<script type="text/javascript" src="../tools/form.js?v=<?php echo filectime('../tools/form.js'); ?>"></script>
	
	<!-- ajax button press -->
	<script type="text/javascript">
		window.form_validate.actions.submit_form = function (the_inputs) {
		    "use strict";
		    console.log(the_inputs);
	        if (!the_inputs.valid) {
	            display_form_error_hints(the_inputs);
	        }
		};
	</script>
</body>

</html>