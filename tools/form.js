/* jslint params */

/*jslint
    this, browser
*/

/*global
    window
    console
*/
(function () {
    "use strict";

    //reset form
    const reset_form = function (this_form, is_form_reset) {
        if (this_form) {
            this_form.querySelectorAll(".hint.show").forEach(function (e) {
                e.classList.remove("show");
            });

            //clear form errors
            let form_error_list = this_form.querySelector(".form-error-list");
            if (form_error_list) {
                form_error_list.classList.remove("show-list");
                form_error_list.innerHTML = "";
            }

            //clear tabs
            if (is_form_reset) {
                let the_tabs = this_form.querySelectorAll(".select-tabs button.selected");
                let selected_options = this_form.querySelectorAll(".select-tabs select option[selected]");

                if (the_tabs.length > 0 && selected_options) {
                    the_tabs.forEach(function (e) {
                        e.classList.remove("selected");
                    });

                    selected_options.forEach(function (e) {
                        e.removeAttribute("selected");
                    });
                }
            }
        }
    };

    //validate singluar input
    const validate_input = function (the_input) {
        const queries = {
            xss: />|<|"|\//gi,
            email: /\S+@\S+\.\S+/,
            andSeek: /&/gi
        };

        the_input.is_valid = 1;

        the_input.value = the_input.value.length > 0
            //? the_input.value.replace(queries.xss, "").replace(queries.andSeek, "and")
            ? the_input.value.replace(queries.xss, "")
            : "";

        switch (the_input.type) {
        case "email":
            the_input.is_valid = queries.email.test(the_input.value)
                ? 1
                : 0;
            break;
        case "checkbox":
        case "radio":
            the_input.checked = the_input.checked
                ? 1
                : 0;
            break;
        default:
            the_input.is_valid = ((the_input.attributes["data-required"] && the_input.value.length) > 0 || !the_input.attributes["data-required"])
                ? 1
                : 0;
        }

        return the_input;
    };

    //validate inputs
    const validate_inputs = function (the_inputs, the_form) {
        if (typeof the_inputs === "object" && the_inputs.length > 0) {
            let return_object = {
                this_form: the_form || null,
                validation_errors: [],
                valid: 1,
                inputs: [],
                input_names: []
            };

            the_inputs.forEach(function (this_input) {
                if (this_input.name && this_input.name.length > 0) {
                    let validated_input = validate_input(this_input);
                    if (validated_input) {

                        //input is valid
                        return_object.valid = (return_object.valid && validated_input.is_valid)
                            ? 1
                            : 0;

                        //not valid, then show message
                        if (!return_object.valid) {
                            let input_parent = this_input.parentNode || false;
                            let hint_box = input_parent
                                ? input_parent.querySelector(".hint")
                                : 0;
                            if (hint_box) {
                                hint_box.classList.add("show");
                                return_object.validation_errors.push({
                                    link: "<a href=\"" + window.location.href.split("#")[0] + "#" + validated_input.name + "\" target=\"_self\">" + hint_box.textContent + "</a>",
                                    content: hint_box.textContent,
                                    id: validated_input.name
                                });
                            }
                        }

                        if (validated_input.type === "select-multiple") {
                            validated_input.selected_items = [];
                            validated_input.querySelectorAll("option[selected]").forEach(function (this_item) {
                                validated_input.selected_items.push(encodeURIComponent(this_item.value));
                            });
                        }

                        //key index
                        return_object.input_names.push(validated_input.name);

                        //add to export
                        let push_to_array = true;

                        if (validated_input.type === "radio" && !validated_input.checked) {
                            push_to_array = false;
                        }

                        if (push_to_array) {
                            return_object.inputs.push({
                                name: validated_input.name,
                                value: encodeURI(validated_input.value),
                                valid: validated_input.selected_items
                                    ? ""
                                    : validated_input.is_valid,
                                type: validated_input.type,
                                items: validated_input.selected_items || 0,
                                checked: validated_input.checked
                                    ? 1
                                    : 0
                            });
                        }
                    }
                }

            });

            return_object.input_json = JSON.stringify(return_object.inputs);

            return return_object;
        }
    };

    //show errors
    const display_form_error_hints = function (data) {
        if (data.this_form) {
            let form_error_list = data.this_form.querySelector(".form-error-list");
            if (form_error_list && data.validation_errors.length > 0) {
                data.validation_errors.forEach(function (this_error) {
                    form_error_list.innerHTML = form_error_list.innerHTML + "<li>" + this_error.link + "</li>";
                });
                form_error_list.classList.add("show-list");
            }
        }
    };

    //forms
    const form_validate = function () {
        const forms = document.querySelectorAll("form");
        const inputs_to_select = "input, textarea, select";
        if (typeof forms === "object" && forms.length > 0) {
            //each form
            forms.forEach(function (this_form) {
                //form submit
                this_form.addEventListener("submit", function (e) {

                    //reset form hints and errors
                    reset_form(this_form);

                    let validated_inputs = validate_inputs(this_form.querySelectorAll(inputs_to_select), this_form);
                    if (!validated_inputs.valid) {
                        e.preventDefault();

                        //display form errors
                        display_form_error_hints(validated_inputs);

                    }
                });

                //buttons
                this_form.querySelectorAll("button").forEach(function (this_button) {
                    this_button.addEventListener("click", function (e) {

                        const data_action = e.target.getAttribute("data-action")
                            ? e.target.getAttribute("data-action")
                            : false;

                        switch (e.target.type) {
                        //reset form and hide hints
                        case "reset":
                            e.preventDefault();
                            this_form.reset();

                            //reset form hints and errors
                            reset_form(this_form, true);

                            break;
                        //standard button
                        case "button":
                            e.preventDefault();
                            if (e.target.getAttribute("data-select-tab")) {
                                e.target.classList.toggle("selected");
                                e.target.parentNode.querySelectorAll("select option").forEach(function (this_option) {
                                    if (this_option.value === e.target.value) {
                                        if (this_option.selected) {
                                            this_option.removeAttribute("selected");
                                        } else {
                                            this_option.setAttribute("selected", true);
                                        }
                                    }
                                });
                            } else if (data_action) {
                                try {
                                    //reset form hints and errors
                                    reset_form(this_form);

                                    //execute action
                                    window.form_validate.actions[data_action](validate_inputs(this_form.querySelectorAll(inputs_to_select), this_form));
                                } catch (error) {
                                    console.log(error + " - (" + e.target.getAttribute("data-action") + " does not exists or is not a callable function)");
                                }
                            }
                            break;
                        }
                    });
                });
            });
        }
    };


    //set global form_validate object
    window.form_validate = {
        actions: {}
    };

    //bind display_form_error_hints to global
    window.display_form_error_hints = display_form_error_hints;

    //run form validator
    form_validate();

}());